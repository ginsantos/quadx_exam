<?php
namespace App\Console\OrderFetcher;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class OrderTransformer
{
    /**
     * @var string
     */
    protected $json;

    /**
     * OrderTransformer constructor.
     * @param string $json
     */
    public function __construct(string $json)
    {
        $this->json = $json;
    }

    /**
     * @return mixed
     */
    public function transform()
    {
        return $this->transformForConsoleOutput();
    }

    /**
     * @return mixed
     */
    protected function transformForConsoleOutput()
    {
        $order = $this->jsonStringToArray();

        $trackingNumber = $order['tracking_number'];
        $container['status'] = $order['status'];
        $container['tracking_number'] = $trackingNumber;

        $tatAscendingDates = $this->sortTatDatesWithTz($order['tat']);
        $container['history'] = $tatAscendingDates;

        $breakdown = $this->getBreakdown($order);
        $container['breakdown'] = $breakdown;

        $fees = $this->getFees($order);
        $container['fees'] = $fees;
        $container['total_fees'] = Collection::make($fees)->sum();
        $container['total'] = $order['total'];

        return $container;
    }

    /**
     * @return mixed
     */
    public function jsonStringToArray()
    {
         return json_decode($this->json, true);
    }

    /**
     * @param array $tat
     * @return array
     */
    protected function sortTatDatesWithTz(array $tat)
    {
        $forSorting = [];
        foreach ($tat as $key => $item) {
            $timestamp = Carbon::make($item['date'])->timestamp;
            $forSorting[$timestamp] = $key;
        }

        ksort($forSorting);
        $tatAscendingDates = [];

        foreach ($forSorting as $key => $item) {
            $timeFormat = Carbon::createFromTimestamp($key)->format('Y-m-d H:i:s.000000');
            $tatAscendingDates[$timeFormat] = $item;
        }

        return $tatAscendingDates;
    }

    /**
     * @param array $tat
     * @return array
     */
    protected function sortTatDates(array $tat)
    {
        $tat = array_flip($tat);
        ksort($tat);

        $tatAscendingDates = [];

        foreach ($tat as $key => $item) {
            $timeFormat = Carbon::createFromTimestamp($key)->format('Y-m-d H:i:s.000000');
            $tatAscendingDates[$timeFormat] = $item;
        }

        return $tatAscendingDates;
    }


    /**
     * @param array $order
     * @return array
     */
    protected function getBreakdown(array $order)
    {
        return [
            'subtotal' => $order['subtotal'],
            'shipping' => $order['shipping'],
            'tax' => $order['tax'],
            'fee' => $order['fee'],
            'insurance' => $order['insurance'],
            'discount' => $order['discount'],
            'total' => $order['total'],
        ];
    }

    /**
     * @param array $order
     * @return array
     */
    protected function getFees(array $order)
    {
        return [
            'shipping fee' => $order['shipping_fee'],
            'insurance_fee' => $order['insurance_fee'],
            'transaction_fee' => $order['transaction_fee'],
        ];
    }
}