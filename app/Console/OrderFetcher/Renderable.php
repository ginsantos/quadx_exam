<?php
namespace App\Console\OrderFetcher;


/**
 * Interface Renderable
 * @package App\Console\Commands
 */
interface Renderable
{
    /**
     * @return mixed
     */
    public function render();
}