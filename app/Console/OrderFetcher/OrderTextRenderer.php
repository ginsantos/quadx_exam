<?php
namespace App\Console\OrderFetcher;


/**
 * Class OrderTextRenderer
 * @package App\Console\Commands
 */
class OrderTextRenderer implements Renderable
{

    /**
     * @var OrderTransformer
     */
    protected $orderTransformer;

    /**
     * @var string
     */
    protected $indentation = " ";

    /**
     * @var string
     */
    protected $newLine = "\n";

    /**
     * OrderTextLogger constructor.
     * @param OrderTransformer $orderTransformer
     */
    public function __construct(OrderTransformer $orderTransformer)
    {
        $this->orderTransformer = $orderTransformer;
    }

    /**
     * @return string
     */
    public function render()
    {
        $order = $this->orderTransformer->transform();

        $allLines = '';
        
        $allLines .= $this->buildTrackingNumber($order);
        $allLines .= $this->buildHistory($order);
        $allLines .= $this->buildBreakDown($order);
        $allLines .= $this->buildFees($order);

        return $allLines . "\n";
    }

    /**
     * @param $order
     * @return string
     */
    private function buildTrackingNumber($order)
    {
        return $this->firstLevel(sprintf("%s : (%s)", $order['tracking_number'], $order['status']));
    }

    /**
     * @param $order
     * @return string
     */
    private function buildHistory($order)
    {
        $lines = '';

        $lines .= $this->secondLevel("history:");

        foreach ($order['history'] as $key => $value) {
            $lines .= $this->thirdLevel("{$key}: $value");
        }

        return $lines;
    }

    /**
     * @param $order
     * @return string
     */
    private function buildBreakDown($order)
    {
        $lines = '';

        $lines .= $this->secondLevel("breakdown:");

        foreach ($order['breakdown'] as $key => $value) {
            $lines .= $this->thirdLevel("{$key}: $value");
        }

        return $lines;
    }

    /**
     * @param $order
     * @return string
     */
    private function buildFees($order)
    {
        $lines = '';

        $lines .= $this->secondLevel("fees:");

        foreach ($order['fees'] as $key => $value) {
            $lines .= $this->thirdLevel("{$key}: $value");
        }

        return $lines;
    }

    /**
     * @param $line
     * @return string
     */
    private function firstLevel($line)
    {

        return "$line{$this->newLine}";
    }

    /**
     * @param $line
     * @return string
     */
    private function secondLevel($line)
    {
        return sprintf("%s%s{$this->newLine}", str_repeat($this->indentation, 2), $line);
    }

    /**
     * @param $line
     * @return string
     */
    private function thirdLevel($line)
    {
        return sprintf("%s%s{$this->newLine}", str_repeat($this->indentation, 3), $line);
    }
}