<?php
namespace App\Console\OrderFetcher;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;

/**
 * Class OrderApiFetcher
 * @package App\Console\Commands
 */
class OrderApiFetcher
{
    /**
     * @var string
     */
    protected $baseUri;

    /**
     * @var array
     */
    protected $orderIds;

    /**
     * OrderApiFetcher constructor.
     * @param string $baseUri
     * @param array $orderIds
     */
    public function __construct(string $baseUri, array $orderIds)
    {
        $this->baseUri = $baseUri;
        $this->orderIds = $orderIds;
    }

    /**
     * @return void
     */
    public function fetchConcurrently()
    {
        $client = new Client([
            'headers' => [
                'X-Time-Zone' => 'Asia/Manila'
            ]
        ]);

        $baseUri = $this->baseUri;
        $orderIds = $this->orderIds;

        $requests = function () use ($orderIds, $baseUri) {
            foreach ($orderIds as $key) {
                yield new Request('GET', $baseUri . $key);
            }
        };

        $accumulator = [];

        $pool = new Pool($client, $requests(), [
            'concurrency' => 5,
            'fulfilled' => function ($response, $index) use (&$accumulator) {
                $jsonData = $response->getBody()->getContents();
                $orderTransformer = new OrderTransformer($jsonData);

                $accumulator['total'][] = $orderTransformer->transform()['total'];
                $accumulator['total_fees'][] = $orderTransformer->transform()['total_fees'];

                $data = new OrderTextRenderer($orderTransformer);

                echo $data->render();
            },
            'rejected' => function ($reason, $index) {
                // this is delivered each failed request
            },
        ]);

        $promise = $pool->promise();
        $promise->wait();

        $grandTotal = Collection::make($accumulator['total'])->sum();
        $grandTotalFees = Collection::make($accumulator['total_fees'])->sum();

        echo "total collections: {$grandTotal}\n";
        echo "total sales: {$grandTotalFees}\n\n";
    }

}