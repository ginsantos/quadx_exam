<?php
/**
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\OrderFetcher\OrderApiFetcher;

/**
 * Class FetchOrdersCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class FetchOrdersCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "fetch:orders";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Summary of orders";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $baseUri = 'https://api.staging.lbcx.ph/v1/orders/';

        $orderIds = [
            '0077-6495-AYUX',
            '0077-6491-ASLK',
            '0077-6490-VNCM',
            '0077-6478-DMAR',
            '0077-1456-TESV',
            '0077-0836-PEFL',
            '0077-0526-EBDW',
            '0077-0522-QAYC',
            '0077-0516-VBTW',
            '0077-0424-NSHE',
        ];

        $order = new OrderApiFetcher($baseUri, $orderIds);

        $order->fetchConcurrently();
    }
}
